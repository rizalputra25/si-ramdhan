package com.luwakdev.siramdhan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.luwakdev.siramdhan.Fragment.BeritaFragment;
import com.luwakdev.siramdhan.Fragment.HomeFragment;
import com.luwakdev.siramdhan.Fragment.ImsakFragment;
import com.luwakdev.siramdhan.Fragment.ProfileFragment;
import com.luwakdev.siramdhan.Fragment.ShalatFragment;
import com.luwakdev.siramdhan.Handler.SQLiteHandler;
import com.luwakdev.siramdhan.Handler.SessionManager;

import java.util.HashMap;

/**
 * Created by Mordecai on 5/27/2017.
 */

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView bottomNavigationView;
    SessionManager sessionManager;
    SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        sessionManager = new SessionManager(getApplicationContext());

        if (!sessionManager.isLoggedIn()) {
            logoutUser();
        }

        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flContent, HomeFragment.newInstance())
                    .commit();
        }

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();
    }

    private void logoutUser() {

        sessionManager.setLogin(false);
        db.deleteUsers();
        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.action_home:
                fragment = HomeFragment.newInstance();
                break;
            case R.id.action_notifications:
                fragment = ShalatFragment.newInstance();
                break;
            case R.id.action_favorites:
                fragment = ImsakFragment.newInstance();
                break;
            case R.id.action_search:
                fragment = BeritaFragment.newInstance();
                break;
            case R.id.action_profile:
                fragment = ProfileFragment.newInstance();
                break;
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContent, fragment)
                .commit();

        return false;
    }
}