package com.luwakdev.siramdhan;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.luwakdev.siramdhan.Config.AppConfig;
import com.luwakdev.siramdhan.Handler.SQLiteHandler;
import com.luwakdev.siramdhan.Model.RestAPI;
import com.luwakdev.siramdhan.Model.Timeline;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyEditText;

/**
 * Created by Mordecai on 5/29/2017.
 */

public class PostActivity extends AppCompatActivity implements Validator.ValidationListener {

    Toolbar toolbar;
    FloatingActionButton fab;
    ImageView image;
    Button buttonChose;
    Bitmap bitmap;
    int PICK_IMAGE_REQUEST = 1;
    Validator validator;
    SweetAlertDialog swal;
    SQLiteHandler db;
    HashMap<String, String> user;
    String encodedImage;

    @NotEmpty
    MyEditText title;

    @NotEmpty
    MyEditText desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        validator = new Validator(this);
        validator.setValidationListener(this);
        db = new SQLiteHandler(this);

        user = db.getUserDetails();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buat Posting Baru");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        title = (MyEditText) findViewById(R.id.etJudul);
        desc = (MyEditText) findViewById(R.id.isipost);
        swal = new SweetAlertDialog(PostActivity.this, SweetAlertDialog.PROGRESS_TYPE);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        image = (ImageView) findViewById(R.id.imageGambar);
        image.setVisibility(View.INVISIBLE);
        buttonChose = (Button) findViewById(R.id.buttonChoose);
        buttonChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //mengambil fambar dari Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //menampilkan gambar yang dipilih dari gallery ke ImageView
                image.setVisibility(View.VISIBLE);
                image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void kosong(){
        image.setImageResource(0);
    }

    @Override
    public void onValidationSucceeded() {
        String email = user.get("email");
        String judul = title.getText().toString();
        String isi = desc.getText().toString();
        String image = getStringImage(bitmap);
        System.out.println("Get Datas : Email "+email+" Judul"+judul+ " Isi"+isi+" Gambar : "+image);

        if (judul != null || isi != null || email != null || image != null) {
            swal.setTitleText("Loading!");
            swal.setContentText("Harap Tunggu...");
            swal.show();
            uploadImage(email, judul, isi, image);
        } else {
            new SweetAlertDialog(PostActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Info!")
                    .setContentText("Judul atau Isi Masih Kosong!")
                    .show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadImage(String email, String judul, String isi, String image) {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        RestAPI request = retrofit.create(RestAPI.class);
        Call<Timeline> timelineCall = request.postingArtikel(email, judul, isi, image);
        timelineCall.enqueue(new Callback<Timeline>() {
            @Override
            public void onResponse(Call<Timeline> call, Response<Timeline> response) {
                Timeline timeline = response.body();
                int status = Integer.parseInt(timeline.getStatus());
                String pesan = timeline.getMsg();

                if (status == 0) {
                    swal.dismiss();
                    new SweetAlertDialog(PostActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText(pesan)
                            .show();
                } else {
                    swal.dismiss();
                    new SweetAlertDialog(PostActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Success!")
                            .setContentText(pesan)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    GoLang();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Timeline> call, Throwable t) {
                new SweetAlertDialog(PostActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    public void GoLang()
    {
        startActivity(new Intent(PostActivity.this, MainActivity.class));
        finish();
    }
}
