package com.luwakdev.siramdhan.Config;

/**
 * Created by Mordecai on 5/27/2017.
 */

public class AppConfig {

    public static String API_URL = "http://ramdhan.luwakdev.com/api/";
    public static String BASE_URL = "http://ramdhan.luwakdev.com/";
    public static final int TIMEOUT = 30; // Dalam Satuan Detik

}
