package com.luwakdev.siramdhan.Intro;

/**
 * Created by Mordecai on 5/27/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.luwakdev.siramdhan.Handler.SQLiteHandler;
import com.luwakdev.siramdhan.Handler.SessionManager;
import com.luwakdev.siramdhan.LoginActivity;
import com.luwakdev.siramdhan.MainActivity;
import com.luwakdev.siramdhan.R;

import java.util.HashMap;

import ws.ChildAnimationExample;
import ws.MyTextView;

public class SliderActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {

    SlideLayout mDemoSlider;
    MyTextView explore;
    SessionManager session;
    SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);

        session = new SessionManager(this);
        db = new SQLiteHandler(this);
        if (session.isLoggedIn() == true) {
            startActivity(new Intent(SliderActivity.this, MainActivity.class));
        }

        explore = (MyTextView)findViewById(R.id.explore);
        explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent boek = new Intent(SliderActivity.this, LoginActivity.class);
                startActivity(boek);
                finish();
            }
        });
//        ********Slider*********

        mDemoSlider = (SlideLayout)findViewById(R.id.slider);

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1", R.drawable.slide_1);


        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    //  .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SlideLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SlideLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new ChildAnimationExample());
        mDemoSlider.setDuration(2000);
        mDemoSlider.addOnPageChangeListener(this);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
