package com.luwakdev.siramdhan;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.luwakdev.siramdhan.Config.AppConfig;
import com.luwakdev.siramdhan.Handler.SQLiteHandler;
import com.luwakdev.siramdhan.Handler.SessionManager;
import com.luwakdev.siramdhan.Model.RestAPI;
import com.luwakdev.siramdhan.Model.User;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mordecai on 5/27/2017.
 */

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    private Button login, register;
    Validator validator;
    SessionManager session;
    SQLiteHandler db;
    SweetAlertDialog swal;

    @NotEmpty
    @Email
    EditText mail;

    @NotEmpty
    @Password(min = 6)
    EditText passw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        validator = new Validator(this);
        validator.setValidationListener(this);

        db = new SQLiteHandler(this);
        session = new SessionManager(this);

        if (session.isLoggedIn() == true) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }

        swal = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);

        // EditText
        mail = (EditText) findViewById(R.id.etEmail);
        passw = (EditText) findViewById(R.id.etPassword);

        login = (Button) findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
        register = (Button) findViewById(R.id.btnRegister);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        String email = mail.getText().toString();
        String password = passw.getText().toString();

        if (email != null || password != null) {
            swal.setTitleText("Loading...");
            swal.setContentText("Harap Tunggu...");
            swal.show();
            requestLogin(email, password);
        } else {
            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Info!")
                    .setContentText("Email atau Password Masih Kosong!")
                    .show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void requestLogin(String email, String password) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        RestAPI request = retrofit.create(RestAPI.class);
        Call<User> user = request.doLogin(email, password);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                int status = Integer.parseInt(user.getStatus());
                String pesan = user.getMsg();

                if (status == 0) {
                    swal.dismiss();
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText(pesan)
                            .show();
                } else {
                    swal.dismiss();
                    final SweetAlertDialog tamvanDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    tamvanDialog.setTitleText("Success!");
                    tamvanDialog.setContentText(""+pesan);
                    tamvanDialog.setConfirmText("Ok");
                    tamvanDialog.show();

                    final String id = user.getId();
                    final String name = user.getName();
                    final String email = user.getEmail();
                    final String alamat = user.getAlamat();
                    final String no_telp = user.getNo_telp();
                    final String image = user.getImage();
                    final String password = user.getPassword();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tamvanDialog.dismissWithAnimation();
                            db.addUser(id, name, email, alamat, no_telp, image, password);
                            GoLang();
                        }
                    }, 1500);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Peringatan !")
                .setContentText("Apakah Anda Ingin Batal Login?")
                .setConfirmText("Ya")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                })
                .setCancelText("Tidak")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    public void GoLang()
    {
        session.setLogin(true);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}
