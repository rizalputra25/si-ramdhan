package com.luwakdev.siramdhan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.luwakdev.siramdhan.Config.AppConfig;
import com.luwakdev.siramdhan.Handler.SQLiteHandler;
import com.luwakdev.siramdhan.Handler.SessionManager;
import com.luwakdev.siramdhan.Model.RestAPI;
import com.luwakdev.siramdhan.Model.User;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mordecai on 5/27/2017.
 */

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    Validator validator;
    SweetAlertDialog swal;

    @NotEmpty
    EditText nama;

    @NotEmpty
    @Email
    EditText mail;

    @NotEmpty
    EditText telp;

    @NotEmpty
    EditText address;

    @NotEmpty
    @Password(min = 6)
    EditText pasw;

    Button register;
    SQLiteHandler db;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        validator = new Validator(this);
        validator.setValidationListener(this);

        db = new SQLiteHandler(this);
        sessionManager = new SessionManager(this);

        if (sessionManager.isLoggedIn() == true) {
            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
        }

        swal = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);

        nama = (EditText) findViewById(R.id.etNama);
        mail = (EditText) findViewById(R.id.etEmail);
        telp = (EditText) findViewById(R.id.etTelp);
        address = (EditText) findViewById(R.id.etAlamat);
        pasw = (EditText) findViewById(R.id.etPassword);

        register = (Button) findViewById(R.id.btnRegister);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

    }

    @Override
    public void onValidationSucceeded() {
        String email = mail.getText().toString();
        String password = pasw.getText().toString();
        String name = nama.getText().toString();
        String alamat = address.getText().toString();
        String no_telp = telp.getText().toString();

        if (email != null || password != null || name != null || alamat != null || no_telp != null) {
            swal.setTitleText("Loading...");
            swal.setContentText("Harap Tunggu...");
            swal.show();
            registerUser(email, password, name, alamat, no_telp);
        } else {
            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Info!")
                    .setContentText("Email atau Password Masih Kosong!")
                    .show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void registerUser(String email, String password, String name, String alamat, String no_telp) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        RestAPI request = retrofit.create(RestAPI.class);
        Call<User> user = request.registerUser(name, alamat, no_telp, email, password);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                int status = Integer.parseInt(user.getStatus());
                String pesan = user.getMsg();

                if (status == 0) {
                    swal.dismiss();
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText(pesan)
                            .show();
                } else {
                    swal.dismiss();
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Success!")
                            .setContentText(pesan)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    GoLang();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG_ERROR",""+t.getMessage().toString());
                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public void GoLang()
    {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }
}
