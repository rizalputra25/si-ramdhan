package com.luwakdev.siramdhan.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luwakdev.siramdhan.R;

/**
 * Created by Mordecai on 5/29/2017.
 */

public class ShalatFragment extends Fragment {

    public static ShalatFragment newInstance() {
        return new ShalatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shalat, container, false);
        return view;
    }
}
