package com.luwakdev.siramdhan.Model;

/**
 * Created by Mordecai on 5/27/2017.
 */

public class User {

    private String id;
    private String name;
    private String email;
    private String password;
    private String alamat;
    private String no_telp;
    private String image;
    private String status;
    private String msg;

    public User(String id, String name, String email, String password, String alamat, String no_telp, String image,
                String status, String msg) {

        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.alamat = alamat;
        this.no_telp = no_telp;
        this.image = image;
        this.status = status;
        this.msg = msg;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
