package com.luwakdev.siramdhan.Model;

/**
 * Created by Mordecai on 5/29/2017.
 */

public class Timeline {

    private String id;
    private String judul;
    private String slug;
    private String isi;
    private String author;
    private String image;
    private String created_at;
    private String updated_at;
    private String status;
    private String msg;

    public Timeline(String id, String judul, String isi, String author, String image, String created_at, String updated_at,
                    String status, String msg) {
        this.id = id;
        this.judul = judul;
        this.isi = isi;
        this.author = author;
        this.image = image;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.status = status;
        this.msg = msg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
