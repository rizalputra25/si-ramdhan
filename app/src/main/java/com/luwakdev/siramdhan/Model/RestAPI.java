package com.luwakdev.siramdhan.Model;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Mordecai on 5/27/2017.
 */

public interface RestAPI {

    @POST("user/login")
    Call<User> doLogin(
            @Query("email") String email,
            @Query("password") String password);

    @POST("user/dregister")
    Call<User> registerUser(@Query("name") String name,
                            @Query("alamat") String alamat,
                            @Query("no_telp") String no_telp,
                            @Query("email") String email,
                            @Query("password") String password);

    @POST("user/post-artikel")
    Call<Timeline> postingArtikel(@Query("email") String email,
                                  @Query("judul") String judul,
                                  @Query("isi") String isi,
                                  @Query("image") String image);
}
